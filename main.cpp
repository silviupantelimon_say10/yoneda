#include <fstream>
#include <iostream>
#include <ranges>
#include <sstream>
#include "include/entity.hpp"
#include "include/reifieable.hpp"
#include "include/csharp.hpp"
#include "include/java.hpp"

using namespace yoneda;
using namespace sayten::types;

int main()
{
    auto saveCSharp = [] (const std::string &result) {
        std::ofstream output("TestClass.cs");
        output << result;

        return result;
    };

    auto saveJava = [] (const std::string &result) {
        std::ofstream output("TestClass.java");
        output << result;

        return result;
    };

    auto logError = [] (const error &err) {
        std::cerr << err;

        return err;
    };

    yoneda::object ob;
    yoneda::entity en;

    ob.name = "TestClass";
    ob.members = {
        {"id", "long"},
        {"test1", "int"},
        {"test2", "string"},
        {"test3", "long"},
        {"test4", "string"}
    };

    en.name = "TestClass2";
    en.members = {
        {"id", "long"},
        {"test1", "int"},
        {"test2", "string"},
        {"test3", "long"},
        {"test4", "string"}
    };
    en.primary_key = { "id", "test2" };

    yoneda::csharp::reify(
        reify_context(ob, {}, {}))
    .bimap(saveCSharp, logError);

    yoneda::csharp::reify(
        reify_context(ob, { {"namespace", "Test.Data"} }, {}))
    .bimap(saveCSharp, logError);

    yoneda::csharp::reify(
        reify_context(ob, {}, { {"mandatory_namespaces", "using System;\r\nusing System.Collections.Generic;\r\nusing System.ComponentModel.DataAnnotations;\r\n"} }))
    .bimap(saveCSharp, logError);

    yoneda::csharp::reify(
        reify_context(en, 
            { {"namespace", "Test.Data"} }, 
            { {"mandatory_namespaces", "using System;\r\nusing System.Collections.Generic;\r\nusing System.ComponentModel.DataAnnotations;\r\n"} }))
    .bimap(saveCSharp, logError);

    yoneda::java::reify(
        reify_context(ob, {}, {}))
    .bimap(saveJava, logError);

    yoneda::java::reify(
        reify_context(ob, {}, { {"mandatory_packages", "import lombok.*;\r\n"} }))
    .bimap(saveJava, logError);

    yoneda::java::reify(
        reify_context(ob, { {"package", "org.test.data"} }, {}))
    .bimap(saveJava, logError);

    yoneda::java::reify(
        reify_context(en, 
        { {"package", "org.test.data"} }, 
        { {"mandatory_packages", "import lombok.*;\r\n"} }))
    .bimap(saveJava, logError);

    return 0;
}