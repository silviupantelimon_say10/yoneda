#ifndef YONEDA_ERROR_HPP
#define YONEDA_ERROR_HPP

#include <fstream>
#include <string>

namespace yoneda
{
    enum class error_code : uint8_t
    {
        NO_ERROR,
        NOT_FOUND_IN_GLOBAL_CONTEXT,
        NOT_FOUND_IN_LOCAL_CONTEXT,
        UNNAMED_OBJECT,
        UNNAMED_MEMBER,
        DUBLICATED_MEMBER,
        UNSUPPORTED_TYPE,
        PRIMARY_KEY_MISSING
    };

    std::string get_error_string(const error_code &code)
    {
        switch (code) {
        case error_code::NOT_FOUND_IN_GLOBAL_CONTEXT:
            return "NOT_FOUND_IN_GLOBAL_CONTEXT";

        case error_code::NOT_FOUND_IN_LOCAL_CONTEXT:
            return "NOT_FOUND_IN_LOCAL_CONTEXT";
        
        case error_code::UNNAMED_OBJECT:
            return "UNNAMED_OBJECT";

        case error_code::UNNAMED_MEMBER:
            return "UNNAMED_MEMBER";

        case error_code::DUBLICATED_MEMBER:
            return "DUBLICATED_MEMBER";

        case error_code::UNSUPPORTED_TYPE:
            return "UNSUPPORTED_TYPE";

        case error_code::PRIMARY_KEY_MISSING:
            return "PRIMARY_KEY_MISSING";
        
        default:
            return "";
        }
    }

    struct error
    {
        error_code code;
        std::string message;

        friend std::ostream &operator<<(std::ostream &os, const error &error_obj)
        {
            return os << "{ error: '" << get_error_string(error_obj.code) << "', message: '" << error_obj.message << "' }\r\n";
        }
    };
}

#endif