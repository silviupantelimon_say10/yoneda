#ifndef YONEDA_ENTITY_HPP
#define YONEDA_ENTITY_HPP

#include <map>
#include "object.hpp"

namespace yoneda
{
    struct entity
    {
        std::string name;
        std::vector<std::tuple<std::string, std::string>> members;
        std::vector<std::string> primary_key;
    };

    template<typename type_t>
    concept entity_abstraction =
    object_abstraction<type_t> &&
    requires (type_t value)
    {
        value.primary_key;
        std::same_as<decltype(value.primary_key), std::vector<std::string>>;
    };
}

#endif