#ifndef YONEDA_VALIDATION_HPP
#define YONEDA_VALIDATION_HPP

#include <string>
#include "object.hpp"
#include "entity.hpp"
#include "reify_context.hpp"
#include "reifieable.hpp"
#include "../../saytenlib/sayten.hpp"

namespace yoneda
{
    using reify_object_context = reify_context<object>;
    template<typename type_t>
    using reifiable_pass_of = reifiable_pass<reify_context<type_t>, string_result>;
    using reifiable_object_pass = reifiable_pass<reify_object_context, string_result>;
    using string_reifiable_object = string_reifiable<reify_object_context>;

    namespace {
        struct member_acc {
            error err;
            std::vector<std::string> names;
        };
    }

    template<object_abstraction object_t, reifiable_pass_of<object_t> cont(const reifiable_pass_of<object_t>&)>
    inline reifiable_pass_of<object_t> forward_error(const reifiable_pass_of<object_t> &pass)
    {
        if (pass.result.has_second())
            return pass;

        return cont(pass);
    }

    template<object_abstraction object_t, template<typename> typename template_function>
    requires callable<template_function<object_t>> &&
    std::same_as<const reifiable_pass_of<object_t>&, typename unwrap_function<template_function<object_t>>::arg_type<0>> &&
    std::same_as<reifiable_pass_of<object_t>, typename unwrap_function<template_function<object_t>>::result_type>
    inline reifiable_pass_of<object_t> forward_error(const reifiable_pass_of<object_t> &pass)
    {
        if (pass.result.has_second())
            return pass;

        return cont(pass);
    }

    template<object_abstraction object_t, string_reifiable<reify_context<object_t>> cont(const reifiable_pass_of<object_t>&)>
    inline string_reifiable<reify_context<object_t>> forward_error(const reifiable_pass_of<object_t> &pass)
    {
       if (pass.result.has_second())
            return string_reifiable<reify_context<object_t>>::pure(pass);

        return cont(pass);
    }

    template<object_abstraction object_t, template<typename> typename template_function>
    requires callable<template_function<object_t>> &&
    std::same_as<const reifiable_pass_of<object_t>&, typename unwrap_function<template_function<object_t>>::arg_type<0>> &&
    std::same_as<string_reifiable<reify_context<object_t>>, typename unwrap_function<template_function<object_t>>::result_type>
    inline string_reifiable<reify_context<object_t>> forward_error(const reifiable_pass_of<object_t> &pass)
    {
       if (pass.result.has_second())
            return string_reifiable<reify_context<object_t>>::pure(pass);

        return cont(pass);
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> validate_object_name(const reifiable_pass_of<object_t> &pass)
    {
        return {
            pass.input,
            !pass.input.get_subject().name.empty() ? pass.result : string_result(error { error_code::UNNAMED_OBJECT, "The object has an empty name!" })
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> validate_member_names(const reifiable_pass_of<object_t> &pass)
    {
        error result = vector_monad(pass.input.get_subject().members)
            .foldr([&object = pass.input.get_subject()] (const std::tuple<std::string, std::string> &member, const member_acc &acc) {
                if (acc.err.code != error_code::NO_ERROR)
                    return acc;

                std::vector<std::string> names = acc.names;
                names.push_back(std::get<0>(member));

                if (std::get<0>(member).empty()) {
                    return member_acc { error { error_code::UNNAMED_MEMBER, "Object \"" + object.name + "\" has a member with an empty name!" }, names };
                }

                return 
                member_acc {
                    vector_monad(acc.names)
                        .filter([&member] (const std::string &name) { return std::get<0>(member) == name; })
                        .extract().empty() ? error { error_code::NO_ERROR, "" } : error { error_code::DUBLICATED_MEMBER, "Object \"" + object.name + "\" has a duplicated member!" },
                    names
                }; 
            }, member_acc { error { error_code::NO_ERROR, "" }, {} }).err;

        return {
            pass.input,
            result.code == error_code::NO_ERROR ? pass.result : string_result(result)
        };
    }

    template<typename entity_t>
    inline reifiable_pass_of<entity_t> validate_primary_key(const reifiable_pass_of<entity_t> &pass)
    {
        if constexpr (entity_abstraction<entity_t>) {
            error result = vector_monad(pass.input.get_subject().primary_key)
            .first([&pass] (const std::string &key) { return !vector_monad(pass.input.get_subject().members)
                .fmap([] (const std::tuple<std::string, std::string> &member) { return std::get<0>(member); }).contains(key); })
            .fmap([] (const std::string &key) { return error { error_code::PRIMARY_KEY_MISSING, "Primary key member \"" + key + "\" is missing from object"}; })
            .or_else_get([] () { return error { error_code::NO_ERROR, "" }; });

            return {
                pass.input,
                result.code == error_code::NO_ERROR ? pass.result : string_result(result)
            };
        } else {
            return pass;
        }
    }

    template<typename object_t>
    inline reifiable_pass_of<object_t> forward(const reifiable_pass_of<object_t> &pass)
    {
        return pass;
    }

    namespace {
        template<bool is_first, typename first_t, typename second_t>
        struct select_altenative {
            using type = first_t;
        };

        template<typename first_t, typename second_t>
        struct select_altenative<false, first_t, second_t> {
            using type = second_t;
        };
    }

    template<bool is_first, typename first_t, typename second_t>
    using select_altenative_t = select_altenative<is_first, first_t, second_t>::type;

    template<bool is_first, typename first_t, typename second_t, 
    reifiable_pass_of<first_t> cont_first(const reifiable_pass_of<first_t> &), reifiable_pass_of<second_t> cont_second(const reifiable_pass_of<second_t> &)>
    inline auto enable_for(const reifiable_pass_of<select_altenative_t<is_first, first_t, second_t>> &pass)
    {
        if constexpr (is_first) {
            return cont_first(pass);
        } else {
            return cont_second(pass);
        }
    }
}

#endif

