#ifndef YONEDA_REIFIABLE_HPP
#define YONEDA_REIFIABLE_HPP

#include <functional>
#include <tuple>
#include <string>
#include "error.hpp"
#include "../../saytenlib/sayten.hpp"

using namespace sayten;
using namespace types;

namespace yoneda
{
    template<typename input_t, typename result_t>
    struct reifiable_pass
    {
        input_t input;
        result_t result;

        reifiable_pass(input_t input, result_t result) : input(input), result(result) {}
    };

    template<typename result_t, typename input_t>
    using reifiable = continuation<result_t, reifiable_pass<input_t, result_t>>;

    using string_result = either<std::string, error>;

    template<typename input_t>
    using string_reifiable = reifiable<string_result, input_t>;
}

#endif