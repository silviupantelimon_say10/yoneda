#ifndef YONEDA_JAVA_HPP
#define YONEDA_JAVA_HPP

#include <string>
#include "object.hpp"
#include "entity.hpp"
#include "reify_context.hpp"
#include "reifieable.hpp"
#include "../../saytenlib/sayten.hpp"

namespace yoneda::java
{
    inline std::string resolve_type_java(const std::string &value)
    {
        return static_cast<char>(::toupper(value[0])) + value.substr(1);
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_imports(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input, 
            pass.result.combine_first([] (const std::string &prev, const std::string &str) { return prev + str + "\r\n"; }, pass.input.get_global("mandatory_packages"))
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_package(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input,
            pass.result.combine_first([] (const std::string &, const std::string &str) { return str; }, 
                pass.input.get_local("package").first_map([] (const std::string &str) { return "package " + str + ";\r\n\r\n"; }))
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_class_start(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input,
            pass.result.first_map([&name = pass.input.get_subject().name] (const std::string &str) { return str + "@Data\r\n@NoArgsConstructor\r\n@AllArgsConstructor\r\npublic class " + name + " {\r\n"; })
        };
    }

    template<object_abstraction object_t>
    inline auto reify_class_member(const object_t &obj)
    {
        if constexpr (!entity_abstraction<object_t>) {
            return [] (const std::tuple<std::string, std::string> &member) {
                return "\tprivate " + resolve_type_java(std::get<1>(member)) + " " + std::get<0>(member) + ";\r\n";
            };
        } else {
            return [&obj] (const std::tuple<std::string, std::string> &member) {
                std::string annotation = !vector_monad(obj.primary_key).contains(std::get<0>(member)) ? "" : "\t@Id\r\n";
                return annotation + "\tprivate " + resolve_type_java(std::get<1>(member)) + " " + std::get<0>(member) + ";\r\n";
            };
        }
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_class_end(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input, 
            pass.result.first_map([] (const std::string &str) { return str + "}\r\n"; })
        };
    }

    template<object_abstraction object_t>
    inline string_reifiable<reify_context<object_t>> reify_members(const reifiable_pass_of<object_t> &pass)
    {
        return string_reifiable<std::vector<std::tuple<std::string, std::string>>>::pure(reifiable_pass(pass.input.get_subject().members, pass.result))
            .fmap([&pass] (reifiable_pass<std::vector<std::tuple<std::string, std::string>>, string_result> members) {
                std::string result = vector_monad<std::tuple<std::string, std::string>>(members.input)
                .fmap(reify_class_member(pass.input.get_subject()))
                .foldl([] (const std::string &a, const std::string &b) { return a + b; }, "");

                return reifiable_pass(pass.input, members.result.first_map([&result] (std::string str) { return str + result; }));
            });
    }

    template<object_abstraction object_t>
    string_result reify(const reify_context<object_t> &obj)
    {
        return string_reifiable<reify_context<object_t>>::pure({ obj, string_result("") })
        .fmap(validate_object_name<object_t>)
        .fmap(forward_error<object_t, validate_member_names>)
        .fmap(forward_error<object_t, validate_primary_key>)
        .fmap(forward_error<object_t, reify_package>)
        .fmap(forward_error<object_t, reify_imports>)
        .fmap(forward_error<object_t, reify_class_start>)
        .bind(forward_error<object_t, reify_members>)
        .fmap(forward_error<object_t, reify_class_end>)
        .run_cont([] (const reifiable_pass_of<object_t> &pass) { return identity(pass.result); })
        .extract();
    }
}

#endif