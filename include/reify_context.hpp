#ifndef YONEDA_REIFY_CONTEXT_HPP
#define YONEDA_REIFY_CONTEXT_HPP

#include <string>
#include <map>
#include "reifieable.hpp"

namespace yoneda
{
    using context_t = std::map<std::string, std::string>;
    using namespace sayten::types;

    template<typename subject_t>
    class reify_context
    {
    private:
        const subject_t subject;
        const context_t local_context;
        const context_t global_context;
    public:
        reify_context(const subject_t &subject, const context_t &local_context, const context_t &global_context) : subject(subject), local_context(local_context), global_context(global_context) {}

        constexpr reify_context replace(const subject_t &subject) const
        {
            return reify_context(subject, local_context, global_context);
        }

        const subject_t &get_subject() const
        {
            return subject;
        }

        string_result get_local(const std::string &key) const
        {
            auto it = local_context.find(key);

            if (it == local_context.end())
                return string_result(error{ error_code::NOT_FOUND_IN_LOCAL_CONTEXT, "Key \"" + key + "\" was not found in local context!" });

            return string_result(it->second);
        }

        string_result get_global(const std::string &key) const
        {
            auto it = global_context.find(key);

            if (it == global_context.end())
                return string_result(error{ error_code::NOT_FOUND_IN_GLOBAL_CONTEXT, "Key \"" + key + "\" was not found in global context!" });

            return string_result(it->second);
        }
    };
}

#endif