#ifndef YONEDA_CSHARP_HPP
#define YONEDA_CSHARP_HPP

#include <string>
#include "object.hpp"
#include "entity.hpp"
#include "reify_context.hpp"
#include "reifieable.hpp"
#include "validation.hpp"
#include "../../saytenlib/sayten.hpp"

namespace yoneda::csharp
{
    inline std::string resolve_type_csharp(const std::string &value)
    {
        return std::string(value);
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_imports(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input, 
            pass.result.combine_first(
                [] (const std::string &, const std::string &str) { return str; },
                pass.input.get_global("mandatory_namespaces").first_map([] (std::string str) { return str + "\r\n"; })
            )
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_namespace_start(const  reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input,
            pass.result.combine_first(
                [] (const std::string &prev, const std::string &str) { return prev + "namespace " + str + " {\r\n"; },
                pass.input.get_local("namespace")
            )
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_class_start(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input, 
            pass.result.first_map([&pass] (const std::string &str) { return str + "\tpublic class " + pass.input.get_subject().name + " {\r\n"; })
        };
    }

    template<object_abstraction object_t>
    inline auto reify_class_member(const object_t &obj)
    {
        if constexpr (!entity_abstraction<object_t>) {
            return [] (const std::tuple<std::string, std::string> &member) {
                return "\t\tpublic " + resolve_type_csharp(std::get<1>(member)) + " " + static_cast<char>(::toupper(std::get<0>(member)[0])) + std::get<0>(member).substr(1) + " { get; set; }\r\n";
            };
        } else {
            return [&obj] (const std::tuple<std::string, std::string> &member) {
                std::string atribute;
                if (obj.primary_key.size() <= 1)
                    atribute = !vector_monad(obj.primary_key).contains(std::get<0>(member)) ? "" : "\t\t[Key]\r\n";
                else
                    atribute = vector_monad(obj.primary_key).get_index(std::get<0>(member)).fmap([] (const size_t &index) { 
                        return "\t\t[Key]\r\n\t\t[Column(Order=" + std::to_string(index + 1) + ")]\r\n"; 
                    }).or_else("");

                return atribute + "\t\tpublic " + resolve_type_csharp(std::get<1>(member)) + " " + static_cast<char>(::toupper(std::get<0>(member)[0])) + std::get<0>(member).substr(1) + " { get; set; }\r\n";
            };
        }
    }

    inline std::string reify_class_member(const std::tuple<std::string, std::string> &member)
    {
        return "\t\tpublic " + resolve_type_csharp(std::get<1>(member)) + " " + static_cast<char>(::toupper(std::get<0>(member)[0])) + std::get<0>(member).substr(1) + " { get; set; }\r\n";
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_class_end(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input,
            pass.result.first_map([] (std::string str) { return str + "\t}\r\n"; })
        };
    }

    template<object_abstraction object_t>
    inline reifiable_pass_of<object_t> reify_namespace_end(const reifiable_pass_of<object_t> &pass)
    {
        return { 
            pass.input,
            pass.result.first_map([] (std::string str) { return str + "}\r\n"; })
        };
    }

    template<object_abstraction object_t>
    inline string_reifiable<reify_context<object_t>> reify_members(const reifiable_pass_of<object_t> &pass)
    {
        return string_reifiable<std::vector<std::tuple<std::string, std::string>>>::pure(reifiable_pass(pass.input.get_subject().members, pass.result))
            .fmap([&pass] (reifiable_pass<std::vector<std::tuple<std::string, std::string>>, string_result> members) {
                std::string result = vector_monad<std::tuple<std::string, std::string>>(members.input)
                .fmap(reify_class_member(pass.input.get_subject()))
                .foldl([] (const std::string &a, const std::string &b) { return a + b; }, "");

                return reifiable_pass(pass.input, members.result.first_map([&result] (std::string str) { return str + result; }));
            });
    }

    template<object_abstraction object_t>
    string_result reify(const reify_context<object_t> &obj)
    {
        return string_reifiable<reify_context<object_t>>::pure({ obj, string_result("") })
        .fmap(validate_object_name<object_t>)
        .fmap(forward_error<object_t, validate_member_names>)
        .fmap(forward_error<object_t, validate_primary_key>)
        .fmap(forward_error<object_t, reify_imports>)
        .fmap(forward_error<object_t, reify_namespace_start>)
        .fmap(forward_error<object_t, reify_class_start>)
        .bind(forward_error<object_t, reify_members>)
        .fmap(forward_error<object_t, reify_class_end>)
        .fmap(forward_error<object_t, reify_namespace_end>)
        .run_cont([] (const reifiable_pass_of<object_t> &pass) { return identity(pass.result); })
        .extract();
    }
}

#endif