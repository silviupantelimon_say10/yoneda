#ifndef YONEDA_OBJECT_HPP
#define YONEDA_OBJECT_HPP

#include <vector>
#include <string>
#include <concepts>

namespace yoneda
{
    struct object
    {
        std::string name;
        std::vector<std::tuple<std::string, std::string>> members;
    };

    template<typename type_t>
    concept object_abstraction =
    requires (type_t value)
    {
        value.name;
        value.members;
        std::same_as<decltype(value.name), std::string>;
        std::same_as<decltype(value.members), std::vector<std::tuple<std::string, std::string>>>;
    };
}

#endif